# <img src="metadata/es-AR/images/icon.png" alt="Icono" height="60"> WiFi Automagico

Guarda todas las redes abiertas a tu alrededor.

<img src="metadata/es-AR/images/phoneScreenshots/principal.jpg" alt="Captura de Pantalla" height="400">
<img src="metadata/es-AR/images/phoneScreenshots/opciones.jpg" alt="Captura de Pantalla" height="400">

## Características:
* Agrega todas redes pulsando un botón.
* Temporizador.
* Filtro para agregar redes cuyos nombres coincidan con un texto elegido.
* Permite ser activado mediante un botón de pánico.


## Advertencia

Conectarte a cualquier red abierta sin tomar medidas de seguridad implica un riesgo para tus datos.

## Donaciones

* Libre de Publicidad.
* Libre de Rastreadores.
* Libre de Telemetría.
* Libre de Código Propietario.
* Libre... de ingresos.


Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



