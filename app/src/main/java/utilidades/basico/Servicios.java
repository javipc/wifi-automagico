package utilidades.basico;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import java.util.List;

/**
 * Javier 2019.
 * editado Junio 2019
 *  Activity -> Context
 */

public class Servicios {


    private Context contexto;

    // verifica si el servicio esta activo
    public Servicios (Context contexto) {
        this.contexto = contexto;
    }



    public List<ActivityManager.RunningServiceInfo> listaServicios () {
        // obtiene una lista de servicios
        return ((ActivityManager) contexto.getSystemService(Context.ACTIVITY_SERVICE)).getRunningServices(Integer.MAX_VALUE);
    }

    public boolean activo(Class servicio) {
        // recorre la lista buscando la clase
        for (ActivityManager.RunningServiceInfo s: listaServicios())
            if(s.service.getClassName().equals(servicio.getName()))
                return true;

        return false;
    }








    // servicios ------------------------------------------------------

    public void iniciarServicio (Class clase) {
        MensajeRegistro.msj(this, "x");
        if (activo(clase))
            return;
        Intent servicio = new Intent(contexto, clase);
        contexto.startService(servicio);
        MensajeRegistro.msj(this, "INICIADO");
    }







    public void detenerServicio (Intent intent) {
        contexto.stopService(intent);
    }

    public void detenerServicio (Class clase) {
        if (!activo(clase))
            return;
        Intent servicio = new Intent(contexto, clase);
        contexto.stopService(servicio);
    }


    // --------------------------------

    public void cerrarAplicacion (Activity activity) {
        // este codigo CIERRA LA APLICACION
        // fuente: https://jcristoballopez.wordpress.com/2015/03/20/anadir-boton-de-salida-con-android-studio/
        activity.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        contexto.startActivity(intent);
    }





    // actividades --------------------------------------------------------



    public Intent iniciarActividad(Class clase) {
        Intent intent = new Intent(contexto, clase);
        contexto.startActivity(intent);
        // contexto.overridePendingTransition(R.anim.zoom_entrada, R.anim.zoom_salida);
        return intent;
    }

}
