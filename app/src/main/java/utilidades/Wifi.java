package utilidades;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Javier  08/06/2019.
 */

public class Wifi {
    private Context contexto;
    private  WifiManager wifiManager;

    public Wifi (Context contexto) {
        this.contexto = contexto;
        wifiManager = (WifiManager) contexto.getSystemService(Context.WIFI_SERVICE);
    }











    // Agrega una nueva red a la configuración
    public void agregar (String SSID) {
        if (existe(SSID))
            return;
        WifiConfiguration configuracion = new WifiConfiguration();
        configuracion.SSID = "\"" + SSID + "\"";
        configuracion.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        configuracion.status = WifiConfiguration.Status.ENABLED;
        configuracion.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        int idRed = wifiManager.addNetwork(configuracion);
        wifiManager.enableNetwork(idRed, false);
        wifiManager.saveConfiguration();

    }



    // Conecta a una red especifica
    public void conectar (String SSID) {
        int idRed = obtenerIdRed(SSID);
        if (idRed == -1)
            return;

        wifiManager.disconnect();
        wifiManager.enableNetwork(idRed, true);
        wifiManager.reconnect();
        wifiManager.enableNetwork(idRed, false);


    }


    private void habilitarRed (String SSID) {
        int idRed = obtenerIdRed(SSID);
        if (idRed == -1)
            return;
        wifiManager.enableNetwork(idRed, false);
    }


    // Obtiene el entero de una red especifica
    private int obtenerIdRed(String SSID) {
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration wifiConfiguration : list)
            if (wifiConfiguration.SSID != null)
                if (wifiConfiguration.SSID.equals("\"" + SSID + "\""))
                    return wifiConfiguration.networkId;

        return  -1;
    }



    public ArrayList<ScanResult> redesDisponibles () {
        wifiManager.startScan();
        List lista;
        lista = wifiManager.getScanResults();
        ArrayList<ScanResult> redes = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++)
            redes.add ((ScanResult) lista.get(i));
        return redes;
    }


    public ArrayList<ScanResult> redesAbiertas () {
        ArrayList<ScanResult> redes = new ArrayList<>();
        for (ScanResult red : redesDisponibles())
            if (red.capabilities.equals("[ESS]"))
                redes.add(red);
        return redes;
    }


    public ArrayList<WifiConfiguration> redesGuardadas () {
        // redes guardadas
        List lista = wifiManager.getConfiguredNetworks();
        WifiConfiguration wifi;
        ArrayList<WifiConfiguration> redes = new ArrayList<>();
        for(int i=0;i<lista.size();i++)
            redes.add((WifiConfiguration) lista.get(i));
        return redes;
    }



    public boolean existe (String SSID) {
        for (WifiConfiguration w : redesGuardadas())
            if (w.SSID.equals("\"" + SSID + "\""))
                return true;

        return false;
    }
}
