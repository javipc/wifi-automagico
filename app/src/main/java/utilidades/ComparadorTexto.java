package utilidades;

/**
 * Javier 2019.
 */

public class ComparadorTexto {
    public boolean mayuscula;
    public boolean comienza;
    public boolean termina;
    public boolean igual;
    public boolean contiene;


    public boolean comparar (String texto, String comparador) {
        if (mayuscula) {
            texto = texto.toLowerCase();
            comparador = comparador.toLowerCase();
        }

        if (contiene)
            return (texto.contains(comparador));

        if (igual)
            if (texto.equals(comparador))
                return true;

        if (comienza)
            if (texto.startsWith(comparador))
                return true;

        if (termina)
            if (texto.endsWith(comparador))
                return true;

        return  false;
    }
}
