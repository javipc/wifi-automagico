package pc.javier.wifiautomagico;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import pc.javier.wifiautomagico.adaptador.Constante;
import pc.javier.wifiautomagico.control.ControlPantallaPrincipal;
import utilidades.basico.EnlaceExterno;
import utilidades.vista.MenuLateral;
import utilidades.vista.PantallaPrincipalConMenuLateral;

public class MainActivity extends PantallaPrincipalConMenuLateral {


    private ControlPantallaPrincipal control;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        control = new ControlPantallaPrincipal(this);
        menuLateral = new MenuLateral(this, R.id.drawer_layout, R.id.nav_view);


    }

    @Override
    public void onResume () {
        super.onResume();
        control.actualizarPantalla ();

    }




    public void clicActivar (View v) {
        control.botonActivar();
        control.actualizarPantalla();

    }



    // Menú lateral -----------------------------------------------------------------

    public void clicMenu (MenuItem item) {
        super.clicMenu(item);

        switch (item.getItemId()) {


            case R.id.menu_ayuda:
                control.iniciarActividad(ActividadAyuda.class);
                break;

            case R.id.menu_registros:
                //control.iniciarActividad(ActividadRegistro.class);
                break;

            case R.id.menu_opciones:
                control.iniciarActividad(ActividadOpciones.class);
                break;

            case R.id.menu_donar:
                donar();
                break;
        }
    }


    public void clicMenu (View v) {
        menuLateral.alternar();
    }


    private void donar (){
        (new EnlaceExterno(this)).abrirEnlace(Constante.urlDonacion);
    }






}
