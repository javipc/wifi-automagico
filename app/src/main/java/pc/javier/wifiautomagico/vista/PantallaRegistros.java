package pc.javier.wifiautomagico.vista;

import android.app.Activity;
import android.widget.ListView;

import pc.javier.wifiautomagico.R;
import pc.javier.wifiautomagico.adaptador.Pantalla;



/**
 * Javier 2019.
 */

public class PantallaRegistros extends Pantalla {
    public PantallaRegistros(Activity activity) {
        super(activity);
    }



    public ListView lista () {
        return (ListView) getView(R.id.registros_lista);
    }


}
