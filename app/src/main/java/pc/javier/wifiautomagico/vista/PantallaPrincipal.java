package pc.javier.wifiautomagico.vista;

import android.app.Activity;
import android.graphics.Color;
import android.view.animation.AnimationUtils;

import pc.javier.wifiautomagico.R;
import pc.javier.wifiautomagico.adaptador.Pantalla;


/**
 * Javier 2019.
 */

public class PantallaPrincipal extends Pantalla {
    public PantallaPrincipal(Activity activity) {
        super(activity);
    }




    public void dibujarBoton (boolean activado) {
        //int idTextViewEstado = R.id.princ_estado;
        int idBoton = R.id.princ_boton;


        int botonActivado = 0;
        int botonDesactivado = 0;


        botonActivado = R.drawable.anilloverde;
        botonDesactivado =R.drawable.botonprincipalredondogris;



        if (activado) {
            // setTextView(idTextViewEstado, R.string.txt_servicio_activo);
            getButton(idBoton).setBackgroundResource(botonActivado);
            getButton(idBoton).setTextColor(Color.BLACK);

            setButtonText(idBoton, R.string.activo);

        } else {
            // setTextView(idTextViewEstado, R.string.txt_servicio_inactivo);
            getButton(idBoton).setTextColor(Color.DKGRAY);
            getButton(idBoton).setBackgroundResource(botonDesactivado);

            setButtonText(idBoton, R.string.activar);
        }



        getButton(idBoton).clearAnimation();
        getButton(idBoton).startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_atras_entrada));


    }




    public void dibujarBoton () {
        //int idTextViewEstado = R.id.princ_estado;
        int idBoton = R.id.princ_boton;


        int botonActivado = 0;


        botonActivado = R.drawable.anilloverde;





        // setTextView(idTextViewEstado, R.string.txt_servicio_activo);
        getButton(idBoton).setBackgroundResource(botonActivado);
        getButton(idBoton).setTextColor(Color.BLACK);
        getButton(idBoton).setTextSize(30);
        getButton(idBoton).setEnabled(true);
        setButtonText(idBoton, R.string.buscar);





        getButton(idBoton).clearAnimation();
        getButton(idBoton).startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_atras_entrada));


    }










    private void mostrarIcono (int idIcono, boolean activo) {
        if (getImageView(idIcono).getVisibility() == visibilidad(activo))
            return;

    }



    /* public void usuario (String usuario) {
        setTextView(R.id.menu_texto_usuario, usuario);
    }*/



}
