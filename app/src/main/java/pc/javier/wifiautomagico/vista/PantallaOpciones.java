package pc.javier.wifiautomagico.vista;

import android.app.Activity;

import pc.javier.wifiautomagico.R;
import pc.javier.wifiautomagico.adaptador.Pantalla;


/**
 * Javier 2019.
 */

public class PantallaOpciones extends Pantalla {
    public PantallaOpciones(Activity activity) {
        super(activity);
    }



    public void setIntervalo (int valor) { setTextView(R.id.opciones_intervalo, String.valueOf(valor)); }
    public int getIntervalo () { return entero(getTexto(R.id.opciones_intervalo)); }

    public void setContienePalabra (String valor) { setTextView(R.id.opciones_contienepalabra, String.valueOf(valor)); }
    public String getContienePalabra () { return getTexto(R.id.opciones_contienepalabra); }



    public boolean getActivacionExterna () { return getToggle(R.id.opciones_permitir_activacion_externa).isChecked();  }
    public void setActivacionExterna (boolean valor) { setToggle(R.id.opciones_permitir_activacion_externa, valor); }



}
