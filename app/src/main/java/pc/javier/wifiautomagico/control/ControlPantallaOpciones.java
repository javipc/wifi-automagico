package pc.javier.wifiautomagico.control;

import android.content.Intent;
import android.view.MenuItem;

import pc.javier.wifiautomagico.R;
import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.vista.PantallaOpciones;
import utilidades.basico.MensajeRegistro;

/**
 * Javier 2019.
 */

public class ControlPantallaOpciones extends Control {

    private Intent intentContactos;
    private PantallaOpciones pantalla;


    public ControlPantallaOpciones(PantallaOpciones pantalla, Preferencias preferencias) {
        super(pantalla, preferencias);

        this.pantalla = pantalla;

        cargarOpciones();
    }




    public void guardar () {

        guardarOpciones();
        getPantalla().finalizarActividad();
    }


    public void cancelar () {
        getPantalla().finalizarActividad();
    }





    private void cargarOpciones() {
        pantalla.setIntervalo(preferencias.getIntervalo());
        pantalla.setContienePalabra(preferencias.getContienePalabra());
        pantalla.setActivacionExterna(preferencias.getActivacionExterna());

    }





    private void guardarOpciones () {
        verificarRegistro();
        preferencias.setIntervalo(pantalla.getIntervalo());
        preferencias.setContienePalabra(pantalla.getContienePalabra());
        preferencias.setActivacionExterna(pantalla.getActivacionExterna());

    }


    public void verificarRegistro () {



    }

    private void versionNoRegistrada() {

        pantalla.snack(R.string.requiereregistro);
    }

    public void mostrarContactos () {
        // ver-
    }



    public void menu (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_ayuda:

                break;

            default:
                // es el botón de atrás
                pantalla.finalizarActividad();
                break;
        }
    }






    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }
}
