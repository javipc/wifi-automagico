package pc.javier.wifiautomagico.control;

import android.app.Activity;
import android.net.wifi.ScanResult;

import java.util.ArrayList;

import pc.javier.wifiautomagico.R;
import pc.javier.wifiautomagico.adaptador.WiFi;
import pc.javier.wifiautomagico.vista.PantallaPrincipal;
import utilidades.Wifi;
import utilidades.basico.MensajeRegistro;
import utilidades.basico.Servicios;


/**
 * Javier 2019.
 */

public class ControlPantallaPrincipal  extends Control {

    private PantallaPrincipal pantalla;

    public ControlPantallaPrincipal(Activity actividad) {
        super(actividad);
        pantalla = new PantallaPrincipal(actividad);
    }






    public void botonActivar () {


        if (preferencias.getIntervalo() <= 0) {
            MensajeRegistro.msj (this, "busqueda unitaria");
            buscarRedes();
            return;
        }

        Class s = ServicioAplicacion.class;
        Servicios servicio = new Servicios(activity);

        if (servicio.activo(s))
            MensajeRegistro.msj (this, "activo");
        else
            MensajeRegistro.msj (this, "inactivo");

        if (servicio.activo(s))
            servicio.detenerServicio(s);
        else
            servicio.iniciarServicio(s);



        if (servicio.activo(s))
            MensajeRegistro.msj (this, "AHORA activo");
        else
            MensajeRegistro.msj (this, "AHORA inactivo");
    }










    private void XXbuscarRedes  () {
        mensajeLog("... buscando ");
        Wifi wifi = new Wifi(activity);
        ArrayList<ScanResult> redes = wifi.redesAbiertas();
        String total = "";
        String contiene = preferencias.getContienePalabra();
        for (ScanResult red : redes) {
            mensajeLog("encontrado: " + red.SSID);
            if (red.SSID.contains(contiene))
                if (!wifi.existe(red.SSID)) {
                    wifi.agregar(red.SSID);
                    total = total + "[" + red.SSID + "] ";
                }
        }
        if (total.equals(""))
            pantalla.snack(R.string.noseencontronuevasredes);
        else
            pantalla.snack("Redes encontradas: " + total);
    }



    private void buscarRedes  () {
        mensajeLog("... buscando ");
        WiFi wifi = new WiFi(activity);
        String contiene = preferencias.getContienePalabra();
        wifi.buscarRedes(contiene);
        String total = wifi.redesAgregadas();

        if (total.equals(""))
            pantalla.snack(R.string.noseencontronuevasredes);
        else
            pantalla.snack("Redes encontradas: " + total);
    }





    private void mensajeLog (String texto) {
        MensajeRegistro.msj ("CONTROL PANTALLA PRINCIPAL: ", texto);
    }




    public void actualizarPantalla () {
        if (preferencias.getIntervalo() <= 0) {
            pantalla.dibujarBoton();
            return;
        }
        Servicios s = new Servicios(activity);
        boolean activo = s.activo(ServicioAplicacion.class);
        pantalla.dibujarBoton(activo);
    }


    public  void iniciarActividad (Class clase) {
        new Servicios(activity).iniciarActividad(clase);
    }
}
