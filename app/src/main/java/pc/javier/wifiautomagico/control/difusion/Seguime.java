package pc.javier.wifiautomagico.control.difusion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.control.ServicioAplicacion;
import utilidades.basico.Servicios;

/**
 * Javier 2019.
 */

public class Seguime extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {


        int intervalo = intent.getIntExtra("intervalo", 10);


        Preferencias preferencias = new Preferencias(context);
        preferencias.setIntervalo (intervalo);

        if (preferencias.getActivacionExterna() == false)
            return;


        Class s = ServicioAplicacion.class;
        Servicios servicio = new Servicios(context);


        if (servicio.activo(s) == false)
            servicio.iniciarServicio(s);


    }


}
