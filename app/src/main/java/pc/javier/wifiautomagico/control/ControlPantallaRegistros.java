package pc.javier.wifiautomagico.control;

import android.app.Activity;
import android.content.Context;

import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.vista.PantallaRegistros;
import utilidades.basico.MensajeRegistro;

/**
 * Javier 2019.
 */

public class ControlPantallaRegistros extends Control {

    private Context contexto;
    private PantallaRegistros pantalla;

    public ControlPantallaRegistros(Activity activity) {
        super(activity);

        contexto = activity;
        this.preferencias = new Preferencias(activity);
        this.pantalla = new PantallaRegistros(activity);



    }








    private void mensajeLog(String texto) {
        MensajeRegistro.msj(this, texto);
    }


}
