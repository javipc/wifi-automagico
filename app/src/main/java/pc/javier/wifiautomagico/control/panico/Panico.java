package pc.javier.wifiautomagico.control.panico;

import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.control.ServicioAplicacion;
import utilidades.basico.Servicios;

/**
 * Created by Javier  on 26/7/2019.
 */

public class Panico extends utilidades.basico.Panico {
    @Override
    public void activar() {



        Preferencias preferencias = new Preferencias(this);

        if (preferencias.getActivacionExterna() == false)
            return;

        if (preferencias.getIntervalo() < 10)
            preferencias.setIntervalo(10);


        Class s = ServicioAplicacion.class;
        Servicios servicio = new Servicios(this);

        if (!servicio.activo(s))
            servicio.iniciarServicio(s);



    }
}
