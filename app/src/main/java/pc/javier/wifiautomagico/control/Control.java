package pc.javier.wifiautomagico.control;

import android.app.Activity;

import pc.javier.wifiautomagico.adaptador.Pantalla;
import pc.javier.wifiautomagico.adaptador.Preferencias;



/**
 * Javier 2019.
 */

public class Control {
    protected Pantalla pantalla;
    protected Preferencias preferencias;
    protected Activity activity;

    public Control (Pantalla pantalla, Preferencias preferencias) {
        this.setPantalla(pantalla);
        this.setPreferencias(preferencias);
        activity = pantalla.getActivity();
    }

    public Control (Activity activity) {
        this.activity = activity;
        pantalla = new Pantalla(activity);
        preferencias = new Preferencias(activity);
    }



    // gets / sets --------------------

    public Pantalla getPantalla() {
        return pantalla;
    }

    public void setPantalla(Pantalla pantalla) {
        this.pantalla = pantalla;
    }

    public Preferencias getPreferencias() {
        return preferencias;
    }

    public void setPreferencias(Preferencias preferencias) {
        this.preferencias = preferencias;
    }






}
