package pc.javier.wifiautomagico.control;

import android.app.Service;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.os.IBinder;

import java.util.ArrayList;

import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.adaptador.WiFi;
import utilidades.Temporizador;
import utilidades.Wifi;
import utilidades.basico.MensajeRegistro;




/*


 */





public class ServicioAplicacion extends Service {


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        iniciarServicio();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        detenerServicio();
        super.onDestroy();
    }






    // -------------------------------------------------------------------



    Preferencias preferencias;
    Temporizador temporizador;

    // inicia este servicio
    private void iniciarServicio () {

        preferencias = new Preferencias(this);




        iniciarTemporizador();

        mensajeLog("TEMPRIZADOR INICIADO");

    }


    // finaliza este servicio
    private void detenerServicio () {

        detenerTemporizador ();
    }






    // ---------------------



    // internet


    private void iniciarTemporizador () {

        int intervalo =  preferencias.getIntervalo ();

        if (intervalo <= 0)
            return;


        temporizador = new Temporizador() {
            @Override
            public void ejecutarTarea() { buscarRedes (); }
        };
        temporizador.setIntervalo(intervalo);
        temporizador.iniciar();
        mensajeLog("temporizador activado ");

    }


    private void XXbuscarRedes  () {
        mensajeLog("... buscando ");
        Wifi wifi = new Wifi(this);
        ArrayList<ScanResult> redes = wifi.redesAbiertas();
        String contiene = preferencias.getContienePalabra();
        for (ScanResult red : redes) {
            mensajeLog("encontrado: " + red.SSID);
            if (red.SSID.contains(contiene))
                if (!wifi.existe(red.SSID)) {
                    wifi.agregar(red.SSID);
                    mensajeLog("AGREGADO>>>>>>>>: " + red.SSID);

                }
        }
    }



    private void buscarRedes  () {
        mensajeLog("... buscando ");
        WiFi wifi = new WiFi(this);
        String contiene = preferencias.getContienePalabra();
        wifi.buscarRedes(contiene);
        // String total = wifi.redesAgregadas();

    }





    private void detenerTemporizador () {
        if (temporizador != null)
            temporizador.detener();
    }


    private void mensajeLog (String texto) {
        MensajeRegistro.msj("Servicio APLICACION", texto);
    }


}
