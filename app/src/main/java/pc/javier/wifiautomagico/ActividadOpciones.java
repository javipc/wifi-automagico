package pc.javier.wifiautomagico;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import pc.javier.wifiautomagico.adaptador.Preferencias;
import pc.javier.wifiautomagico.control.ControlPantallaOpciones;
import pc.javier.wifiautomagico.vista.PantallaOpciones;


public class ActividadOpciones extends AppCompatActivity {

    ControlPantallaOpciones control;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.opciones);

        PantallaOpciones pantalla = new PantallaOpciones(this);
        Preferencias preferencias = new Preferencias(this);
        control = new ControlPantallaOpciones(pantalla, preferencias);

    }




    public void verificarRegistro (View v) {
        control.verificarRegistro ();
    }

    public void cancelar (View v) {
        control.cancelar ();
    }


    public void guardar (View v) {
        control.guardar();
    }







    // Menú superior ---------------
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_opciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        control.menu (item);
        return true;
    }



}
