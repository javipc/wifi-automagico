package pc.javier.wifiautomagico.adaptador;

import android.content.Context;
import android.net.wifi.ScanResult;

import java.util.ArrayList;

import utilidades.basico.MensajeRegistro;

/**
 * Javier 2019.
 */

public class WiFi extends utilidades.Wifi {

    public WiFi(Context contexto) {
        super(contexto);
    }


    ArrayList<String> totalRedesAgregadas = new ArrayList<String>();
    public String redesAgregadas () {
        String total  = "";
        for (String red : totalRedesAgregadas)
            total = total + "[" + red + "] ";
        return total;
    }


    public void buscarRedes  (String contiene) {
        mensajeLog("... buscando ");
        totalRedesAgregadas.clear();

        for (ScanResult red : redesAbiertas()) {
            mensajeLog("encontrado: " + red.SSID);
            if (red.SSID.contains(contiene))
                if (!existe(red.SSID)) {
                    agregar(red.SSID);
                    totalRedesAgregadas.add(red.SSID);
                }
        }
    }

    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }
}
