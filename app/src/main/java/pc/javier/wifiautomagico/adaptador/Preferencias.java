package pc.javier.wifiautomagico.adaptador;

import android.content.Context;


/**
 * Javier 2019.
 * Manejador de preferencias
 * almacena y recupera preferencias de la aplicación
 */

public class Preferencias extends utilidades.basico.Preferencias {


    public Preferencias(Context contexto) {
        super(contexto);
    }

    /*public Preferencias(Context contexto, String nombrePreferencias) {
        super(contexto, nombrePreferencias);
    }*/



    public void guardar(TipoPreferencia clave, String valor) {
        guardar(clave.toString(), valor);
    }

    public void guardar(TipoPreferencia clave, int valor) {
        guardar(clave.toString(), valor);
    }

    public void guardar(TipoPreferencia clave, boolean valor) {
        guardar(clave.toString(), valor);
    }

    public void guardarLong(TipoPreferencia clave, long valor) {
        guardarLong(clave.toString(), valor);
    }


    public int obtenerInt(TipoPreferencia clave) {
        return obtenerInt(clave.toString());
    }

    public String obtenerString(TipoPreferencia clave) {
        return obtenerString(clave.toString());
    }

    public boolean obtenerBoolean(TipoPreferencia clave) {
        return obtenerBoolean(clave.toString());
    }

    public long obtenerLong(TipoPreferencia clave) {
        return obtenerLong(clave.toString());
    }


    public void borrar(TipoPreferencia clave) {
        borrar(clave.toString());
    }






    public enum TipoPreferencia {


        intervalo,
        contienepalabra,

        activacionExterna


    }














    public int getIntervalo () { return obtenerInt (TipoPreferencia.intervalo); }
    public void setIntervalo (int valor) { guardar (TipoPreferencia.intervalo, valor); }


    public String getContienePalabra () { return obtenerString (TipoPreferencia.contienepalabra); }
    public void setContienePalabra (String valor) { guardar (TipoPreferencia.contienepalabra, valor); }




    public boolean getActivacionExterna () { return obtenerBoolean(TipoPreferencia.activacionExterna);  }
    public void setActivacionExterna (boolean valor) { guardar(TipoPreferencia.activacionExterna, valor); }





}
