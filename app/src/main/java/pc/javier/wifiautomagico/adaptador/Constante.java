package pc.javier.wifiautomagico.adaptador;

import pc.javier.wifiautomagico.BuildConfig;

/**
 * Javier 2019.
 *  configuraciones y parámetros constantes
 */

public final class Constante {
    public final static String version = BuildConfig.VERSION_NAME;
    public final static String fechaVersion = "Junio 2019";

    public final static String urlDonacion = "https://gitlab.com/javipc/mas";
    public final static String urlSitio = "https://gitlab.com/javipc/wifi-automagico";

}
